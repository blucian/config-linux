# config-linux

## Linux configuration machine

#### Ubuntu 18.04 LTS
#### Anaconda version 5.3.0 ++
 - https://www.anaconda.com/
#### Git
 - https://git-scm.com/book/pt-br/v1/Primeiros-passos-Instalando-Git
#### R-base; R-dev
 - https://cloud.r-project.org/bin/linux/ubuntu/README.html
#### Apache NiFi
 - http://nifi.apache.org/download.html
#### Atom/sublime
 - https://atom.io/
 - https://www.sublimetext.com/3
#### OpenSSH service
 - https://help.ubuntu.com/lts/serverguide/openssh-server.html.en
#### Virtual box with company image and powerBI
 - https://www.virtualbox.org/wiki/Linux_Downloads
#### Google Cloud SDK
 - https://cloud.google.com/sdk/install


